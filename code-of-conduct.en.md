# Code of Conduct
<!-- introduction -- who we are, our goals and values} -->
Welcome to f.u.c.k. (Feminismus und Computerkram/ Feminism and computer stuff); we are a group of women, inter, trans, non-binary and agender people that share an interest in computers and feminism. We meet in online spaces, as we are an online hack space. We try to be a safer space. We prioritize marginalized people’s safety over privileged people’s comfort.


## Whom this space is for
Our goal is to provide a safer space in which women, inter, trans, non-binary and agender people with an interest in computing and feminism can come together.  
No coding skills or IT knowledge are required to join our group. Newbies are perfectly welcome and encouraged to join!
Our group emphasizes intersectional, queer-feminist views. <!-- Don't know how to express this any better... how is this? -->


## Whom this space is not for
A key part of this space is the exclusion of endo cis men, in order to give people of all other gender identities a safer hack space to meet.  
Additionally, we exclude TERFs; anyone who is transphobic, racist, sexist, ableist, fatphobic, antisemitic, or right-wing; and anyone who doesn't respect human rights in any other way. We want this to be more than just a phrase. We want to create an environment in which people activly work against any form of discrimination and reflect upon our own unconscious biases and discriminatory behaviors or views.


# Rules
In general, please don't be a jerk, please respect people, and please take it seriously if somebody asks you to stop a certain behaviour. Please respect people's boundaries.

## You are encouraged to
- participate, create this space and bring in your ideas.
- ask questions.
- ask twice if you don't understand something at first.
- specify your own boundaries.
- ask for support if you feel overwhelmed or overburdened.


## You are expected to
- respect the boundaries of others.
- keep in mind that boundaries of others can change. Something that is fine on one day, can be too much on another day.
- use the correct pronouns when referring to people.
- be mindful of the space you're taking up and make space for people who are quieter.
- not interrupt people.
- be mindful of topics that might be emotionally sensitive for people (eg. drugs, violence, mental health, climate doom, ...); when in doubt, ask if everyone is okay with you talking or sharing content about it.


## You are not allowed to <!-- you shouldn't?-->
- act discriminatory in any form.
- make unsolicited comments about people's bodies, and even if somebody brings up a topic themselves. Please take extra care to be respectful.
- to spread conspiracy beliefs or fake news.
- endanger the place. For example, do not take part in any illegal activity within or in connection to the space.
- take photos, screenshots, videos, or other analogue or digital recordings in any form without the explicit consent of everyone involved.
- publish or forward parts of the chat.
- out other people as being a member of a certain group (eg. trans) without their consent.
- spread propaganda or content with positive references to facist regimes or parties, whether present or historical.
- publicly publish links to video chats.

# Conflict resolution and consequences
Please be receptive if someone tells you that something you have said or done is hurtful. If you don't understand why something is hurtful, be open to suggestions for reading material or explanations, but there shouldn't be any expectation that the person being hurt is the person to explain the situation to you.

If a discussion becomes heated, sometimes the best way forward might be to stop and think. Afterwards, it can be decided if the discussion should be continued or if this is not necessary or helpful.

If a situation becomes heated and a person is being hurt, however, immediate action is still necessary.

If you face harassment or a member of the community is acting in a harmful way, or you need support for any other reason, you can approach another member of the group for help. People who can be approached in this context will have a 🐻 emoji on their display name in the *main* Matrix channel. You may also raise concerns with the whole community.

Our goal is to create a safe environment for everyone. We reserve the right to expel offenders without giving justification. However, we consider exclusion to be a last resort that we'd prefer not to have to take.

We want to create a sustainable community from a perspective of transformative justice.
