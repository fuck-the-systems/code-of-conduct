# Code of Conduct
Willkommen bei f.u.c.k. (Feminismus und Computerkram)! Wir sind eine Gruppe von FLINTA* (Frauen, Lesben, Inter, Non-Binary, Trans, Agender), die sich für Feminismus und Computerkram interessieren. Wir treffen uns online und sind ein online Hackspace. Unser Ziel ist es, einen sicheren Raum zu schaffen, der marginalisierte Personen schützt und ihre Bedürfnisse über den Komfort privilegierter Personen stellt.


## Für wen dieser Hackspace ist
Unser Ziel ist es, einen Safer Space für FLINTA Personen mit Interesse an Feminismus und Computerkram zu schaffen, in dem sich getroffen und ausgetauscht werden kann. Es sind keine Programmierkenntnisse oder IT-Wissen notwendig um mitzumachen. IT-Neulinge sind super willkommen und können sehr gerne dazu kommen.
Unsere Gruppe identifiziert sich mit intersektionalem Queer-Feminismus.


## Für wen dieser Hackspace NICHT ist
Ein wesentlicher Grundsatz ist der Ausschluss von endo-cis-Männern, um Personen aller anderen Geschlechter einen geschützten Raum zu bieten. Zusätzlich schließen wir TERFs aus sowie alle Personen die transfeindliche, rassistische, sexistische, ableistische, fettenfeindliche, antisemitische, rechts-radikale oder anderweitig diskriminierende Einstellungen vertreten. Wir möchten, dass dies nicht nur leere Worte sind. Unser Ziel ist es, eine Umgebung zu schaffen, in der Menschen aktiv gegen jede Form von Diskriminierung vorgehen, ihre eigenen Privilegien reflektieren und unbewusste diskriminierende Denk- und Verhaltensweisen erkennen.

# Regeln
Generell, sei freundlich, rücksichtsvoll und respektiere andere Personen. Und nimm es ernst, wenn eine Person dich bittet ein bestimmtes Verhalten zu unterlassen. Und respektiere die Grenzen anderer.

## Du bist ermutigt dazu
- mitzumachen, den Hackspace aktiv zu gestalten und eigene Ideen einzubringen.
- Fragen zu stellen.
- erneut zu fragen, wenn du etwas beim ersten Mal nicht verstanden hast.
- deine eigene Grenzen festzulegen und mitzuteilen.
- um Hilfe zu bitten, besonders wenn du überfordert oder überlastet bist.

## Es wird erwartet, dass du
- die Grenzen anderer respektierst.
- beachtest, dass die Grenzen anderer sich ändern können. Etwas, das an einem Tag OK oder cool war, kann an einem anderen Tag zu viel sein.
- die korrekten Pronomen für andere Personen benutzt.
- achtsam bist wie viel Raum du einnimmst und ruhigeren Personen Raum lässt.
- andere Personen nicht unterbrichst.
- achtsam bist mit Themen, die für andere Personen schwierig sein könnten (z.B. Drogen, Gewalt, psychische Gesundheit, Klimawandel, ...). Wenn du dir unsicher bist, ob ein Thema OK ist, frag die anderen Personen mit denen du sprichst oder die Inhalte teilst.

## Es ist nicht erwünscht, dass du
- dich diskriminierend in jeglicher Form verhältst oder äußerst.
- ungebetene Kommentare über die körperliche Erscheinung anderer machst, auch wenn die Person das Thema selbst beginnt. Bitte sei hier besonders rücksichtsvoll.
- Verschwörungsphantasien oder Geschwurbel äußerst oder teilst.
- diesen Ort in Gefahr bringst. Führe keine illegalen Aktivitäten aus, die mit diesem Ort in Verbindung gebracht werden können.
- Photos, Screenshots, Videos oder andere analoge oder digitale Aufnahmen erstellst ohne die explizite Zustimmung aller Beteiligten.
- Teile des Chatverlaufs veröffentlichst und verschickst.
- andere outest (z.B. als trans) ohne deren Einverständnis.
- Propaganda von oder Inhalte mit positiver Bezugnahme zu faschistischen Regimen oder Parteien der Gegenwart und Vergangenheit verbreitest.
- öffentlich Links zu unseren Video Meetings veröffentlichst.

# Konfliktlösungen und Kosequenzen
Bitte sei empfänglich, wenn eine Person dir mitteilt, dass etwas, das du gesagt oder getan hast, verletzend war. Wenn du nicht verstehst wieso etwas verletzend war, dann sei offen für Verweise auf Erklärungen und Informationsmaterialien. Jedoch solltest du keine Erwartung haben, dass die Person, die verletzt wurde, dir alles erklärt.

Wenn eine Diskussion hitzig wird, ist es manchmal am besten, innezuhalten und nachzudenken. Danach kann entschieden werden, ob die Diskussion weitergeführt werden soll oder ob das nicht notwendig oder zielführend ist.

Wenn eine Situation angespannt ist und eine Person verletzt wird, ist ein unmittelbares Handeln notwendig und es sollte nicht abgewartet werden.

Wenn du belästigt oder diskriminiert wirst, eine andere Person sich verletzend verhält oder du aus einem anderen Grund Unterstützung brauchst, kannst du dich an Personen aus der Gruppe wenden. Personen, die hierfür offen und erreichbar sind, haben ein 🐻 Emoji an ihrem Anzeigenamen im *main* Matrix Raum. Du kannst Dinge aber auch einfach vor der ganzen Gruppe ansprechen.

Unser Ziel ist es eine sichere Umgebung für alle zu schaffen. Daher behalten wir uns das Recht vor Personen, die sich übergriffig verhalten, auszuschließen ohne uns rechtfertigen zu müssen. Allerdings halten wir den Ausschluss für das letzte Mittel, zu dem wir nicht greifen möchten so lange sich andere Lösungen finden lassen.

Wir wollen eine stabile und langlebige Gemeinschaft bilden und wollen hierfür das Konzept der transformativen Gerechtigkeit als Grundlage nutzen.
